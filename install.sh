#!/usr/bin/env bash

if [ "$EUID" -eq 0 ]
    then echo "Don't run this script as root"
    exit 1
fi

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
INSTALL_DIR=~/.local/share/avoidmenu

mkdir -p "$INSTALL_DIR"
mkdir -p ~/.local/bin

cp "$SCRIPT_DIR"/avoidmenu-worker "$INSTALL_DIR"/
cp "$SCRIPT_DIR"/avoidmenu-ctrl "$INSTALL_DIR"/
cp "$SCRIPT_DIR"/avoidmenu-daemon "$INSTALL_DIR"/
cp "$SCRIPT_DIR"/defaults.conf "$INSTALL_DIR"/

ln -s "$INSTALL_DIR"/avoidmenu-ctrl ~/.local/bin/avoidmenu

echo "avoidmenu has been installed in $INSTALL_DIR and avoidmenu-ctrl is symlinked in ~/.local/bin"

if [[ ":$PATH:" == *":$HOME/.local/bin:"* ]]; then
  echo "~/.local/bin is in your path and can now be run with 'avoidmenu [options] <COMMAND>'"
else
  echo "Your path is missing ~/.local/bin, you might want to add it."
fi
