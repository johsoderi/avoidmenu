# avoidmenu

Avoid accidentally unhiding the macOS menu bar when using an app in fullscreen

## Description
<details><summary>Show gif: The problem</summary>
![problem](https://me.qr.ax/random/problem.gif)
</details>

<details><summary>Show gif: The "solution"</summary>
![solution](https://me.qr.ax/random/solution.gif)
</details>

The auto-hiding menu bar in macOS fullscreen mode can be a pain in the rear end when you're using an app with its own menues/tabs/anything at the top of the screen.
Searching for a solution to this problem, the only one I found that did anything for me was to use the Python module `autopy`, specifically the `autopy.mouse` part. The way it can prevent the menu bar from unhiding is to literally move the cursor back down whenever it comes close to the trigger area.

This method is of course janky af, and not an ideal solution in any shape or form. That said, it does the job so I've made an attempt at daemonizing it.

A problem I had with autopy was that I couldn't get it to run in python3 on my M1 Mac, and while the python2.7 module worked fine I didn't like to depend on it. Soon enough though, I noticed that the python module was based on the excellent rust extension `autopilot-rs` by the same author, so I rewrote 'avoidmenu-worker.py' in rust, which removed any external dependencies.

Tested with bash 5 in MacOS 13.5 Ventura on M1 Max MBP.

## Installation

### Clone this repo
```
$ git clone https://gitlab.com/johsoderi/avoidmenu.git
$ cd avoidmenu
```

### Run installation script
```
$ ./install.sh
```

## Usage
<center><img src="screenshots/status4.png" alt="status" width="500"></center>

```
usage: avoidmenu [OPTIONS] start|restart [APPNAME]
  or   avoidmenu add|remove APPNAME
  or   avoidmenu status|stop|reset

options:
  -a int        app switch check delay in ms - higher means lower check rate (default: 1000)
  -d int        bounce step distance in pixels - higher means faster bounce-back (default: 2)
  -c int        cursor check delay in ms - lower is smoother, but harder on CPU (default: 3)
  -t int        top limit - pixel count from top of screen, sets no-go zone (default: 6)
  -b int        bottom limit - pixel count from bottom of screen, sets no-go zone (default: 0)
  -l int        left limit - pixel count from left of screen, sets no-go zone (default: 2)
  -r int        right limit - pixel count from right of screen, sets no-go zone (default: 0)
  -q            quiet mode - suppress non-error messages
  -z            no-swag ascii mode for boring people and dumb terminals
  -s            save current settings to ~/.config/avoidmenu/avoidmenu.conf

commands:
  start                 start the daemon, optionally add an app to trigger app list
  stop                  stop the daemon
  restart               restart the daemon
  status                print daemon status, settings, and the trigger app list
  reset                 stop the daemon & reset all settings, then start it again
  add APPNAME           add an app to trigger app list
  remove APPNAME|all    remove an app from trigger app list, or empty list with 'all'

examples:
  avoidmenu -c 1 -d 10 -a 200 -t 30 start Google Chrome.app
  avoidmenu start
  avoidmenu stop
  avoidmenu restart
  avoidmenu status
  avoidmenu reset
  avoidmenu add iTerm.app
  avoidmenu remove Finder.app
```

## Build avoidmenu-worker from source

### Install rust & rustup
```
# Instructions: https://doc.rust-lang.org/cargo/getting-started/installation.html
# tl;dr:
$ curl https://sh.rustup.rs -sSf | sh
```

### Clone this repo
```
$ git clone https://gitlab.com/johsoderi/avoidmenu.git
$ cd avoidmenu
```

### Run build script
```
$ ./build.sh
```

## Authors and acknowledgments
By johsoderi, dec 2021 - oct 2023

Built with [autopilot-rs](https://github.com/autopilot-rs/autopilot-rs) by Michael Sanders

Inspired by [Jarda Dvořák's answer](https://apple.stackexchange.com/a/394646/442504) to the apple.stackexchange.com question [Make the Menu Bar never show while in Full Screen](https://apple.stackexchange.com/questions/70985/make-the-menu-bar-never-show-while-in-full-screen)
