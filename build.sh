#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

INSTALL_MSG="Install rust and rust-cargo: https://doc.rust-lang.org/cargo/getting-started/installation.html
tl;dr:
curl https://sh.rustup.rs -sSf | sh"

cargo --version; cargo_status=$?
if [ "${cargo_status}" -ne 0 ]; then
    echo "rust-cargo not found. $INSTALL_MSG"
    exit 1
fi

rustup --version; rustup_status=$?
if [ "${rustup_status}" -ne 0 ]; then
    echo "rustup not found. $INSTALL_MSG"
    exit 1
fi

rustc --version; rustc_status=$?
if [ "${rustc_status}" -ne 0 ]; then
    echo "rustc not found. $INSTALL_MSG"
    exit 1
elif

# Add rust targets for Intel and ARM Macs:
rustup target add aarch64-apple-darwin
rustup target add x86_64-apple-darwin

# Clone autopilot-rs
git clone https://github.com/autopilot-rs/autopilot-rs.git "$SCRIPT_DIR"/src/autopilot-rs

# Compile avoidmenu-worker for both Intel and ARM Macs:
cd "$SCRIPT_DIR"/src/avoidmenu-worker
cargo build --target=aarch64-apple-darwin --release
cargo build --target=x86_64-apple-darwin --release

# Combine the two binaries into one universal binary:
cargo install cargo-lipo
rm "$SCRIPT_DIR"/avoidmenu-worker
lipo -create "$SCRIPT_DIR"/src/avoidmenu-worker/target/aarch64-apple-darwin/release/avoidmenu-worker "$SCRIPT_DIR"/src/avoidmenu-worker/target/x86_64-apple-darwin/release/avoidmenu-worker -output "$SCRIPT_DIR"/avoidmenu-worker

echo "Finished building avoidmenu-worker."
