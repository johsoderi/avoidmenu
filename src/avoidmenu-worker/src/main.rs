extern crate autopilot;
use std::env;
use std::path::Path;
fn avoidloop() -> Result<(), autopilot::mouse::MouseError> {
    let args: Vec<String> = env::args().collect();
    let dirpath = Path::new(&args[1]);
    let update_delay = args[2].parse::<u64>().unwrap();
    let limit_top = args[3].parse::<f64>().unwrap();
    let limit_bottom = args[4].parse::<f64>().unwrap();
    let limit_left = args[5].parse::<f64>().unwrap();
    let limit_right = args[6].parse::<f64>().unwrap();
    let bounce_speed = args[7].parse::<f64>().unwrap();
    let screen_size = autopilot::screen::size();
    let limit_bottom_real = screen_size.height - limit_bottom;
    let limit_right_real = screen_size.width - limit_right;
    loop {
        let curr_pos = autopilot::mouse::location();
        let x = curr_pos.x;
        let y = curr_pos.y;
        if dirpath.exists() {
            if y < limit_top {
                let y = y + bounce_speed;
                autopilot::mouse::move_to(autopilot::geometry::Point::new(x, y))?;
            }
            if x < limit_left {
                let x = x + bounce_speed;
                autopilot::mouse::move_to(autopilot::geometry::Point::new(x, y))?;
            }
            if y > limit_bottom_real {
                let y = y - bounce_speed;
                autopilot::mouse::move_to(autopilot::geometry::Point::new(x, y))?;
            }
            if x > limit_right_real {
                let x = x - bounce_speed;
                autopilot::mouse::move_to(autopilot::geometry::Point::new(x, y))?;
            }
        }
        std::thread::sleep(std::time::Duration::from_millis(update_delay));
    }
}
fn main() {
    avoidloop().expect("Unable to move mouse");
}
